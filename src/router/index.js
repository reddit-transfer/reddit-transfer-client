import Vue from 'vue';
import Router from 'vue-router';
import Landing from '@/components/Landing';
// import RequestToken from '@/components/RequestToken';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing,
    },
    // {
    //   path: '/requestToken/',
    //   name: 'request',
    //   component: RequestToken,
    // },
  ],
});
